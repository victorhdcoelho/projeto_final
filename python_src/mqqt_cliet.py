import paho.mqtt.client as mqtt
import json
from datetime import datetime
from gpio_utils import GPIOUtils
from alarm import AlarmUtils



class MqttClient:
    def __init__(self):
        self.client = None
        self.available_topics = []
        self.connected_brokers = []
        self.disp = []
        self.gpios = GPIOUtils()
        self.gpios.init_all_gpios()
        self.apa = self.gpios.get_all_status()
        self.alarm_cli = AlarmUtils()
        self.alarm = "OFF"
        self.save = [self.available_topics, self.connected_brokers, self.disp ]

    def save_command(self, data):
        with open("log.csv", 'a') as f:
            f.write(data)
    
    def mac_verify(self, msg):
        """
         Method to verify if mac in connect and att time. 
        """
        if msg.topic == "front/mac":
            info = json.loads(msg.payload.decode())
            flag = 0
            for each in self.connected_brokers:
                if info["mac"] == each["mac"]:
                    each["date"] = datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
                    flag = 1
            if flag == 0:
                self.connected_brokers.append({"mac": info["mac"], "date": datetime.now().strftime("%d/%m/%Y, %H:%M:%S")})   

    def init_component(self, msg):
        """
        Method to register new esp on sistem.
        """
        if "fse2020/150129815/dispositivos/" in msg.topic:
            topic = msg.topic
            mac = topic.split("/")[3]
            print("cadastrando mac: {}".format(mac))
            self.save_command("{}, {}\n".format("cadastrando mac: {}".format(mac), datetime.now().strftime("%H:%M:%S")))
            self.mac_verify(msg)
            flag = 0
            for each in self.connected_brokers:
                if mac == each["mac"]:
                    each["date"] = datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
                    flag = 1
            if flag == 0:
                self.connected_brokers.append({"mac": mac, "date": datetime.now().strftime("%H:%M:%S")})

            
    def verify_room(self, msg):
        """
        Verify room msg.
        """
        msg_json = json.loads(msg.payload.decode())
        if "fse2020/150129815/dispositivos/" in msg.topic:
            if "room" in msg_json.keys():
                mac = msg.topic
                mac = mac.split("/")[3]
                topic = "fse2020/150129815/{}/#".format(msg_json["room"])
                new_msg = {"mac": mac, "room": msg_json["room"], "temperatura": 0.0, "umidade": 0.0, "status": "OFF", "led": "OFF"}
                self.disp.append(new_msg)
                self.subscribe_topic(topic)

    def refresh_room(self, msg):
        """
        Refresh temperature, humity and status of rooms
        """
        room_topics = ["fse2020/150129815/{}/".format(room["room"]) for room in self.disp]
        for topic_room in room_topics:
            if msg.topic.startswith(topic_room):
                room = msg.topic
                room = room.split("/")[2]
                msg_json = json.loads(msg.payload.decode())  
                for each in self.disp:    
                    if "umidade" in msg.topic and each["room"] == room:
                        each["umidade"] = msg_json["umidade"]
                        for broker in self.connected_brokers:
                            if broker["mac"] == each["mac"]:
                                broker["date"] = datetime.now().strftime("%d/%m/%Y, %H:%M:%S")

                    if "temperatura" in msg.topic and each["room"] == room:
                        each["temperatura"] = msg_json["temperatura"]
                        for broker in self.connected_brokers:
                            if broker["mac"] == each["mac"]:
                                broker["date"] = datetime.now().strftime("%d/%m/%Y, %H:%M:%S")

                    if "estado" in msg.topic and each["room"] == room:
                        each["status"] = msg_json["entrada"]
                        if "saida" in msg_json.keys():
                            each["led"] = msg_json["saida"]
                        for broker in self.connected_brokers:
                            if broker["mac"] == each["mac"]:
                                broker["date"] = datetime.now().strftime("%d/%m/%Y, %H:%M:%S")

    def turn_led(self, msg):
        """
        Change logical led value
        """
        if msg.topic == "front/turnled":
            msg_json = json.loads(msg.payload.decode())
            self.save_command("{}, {}\n".format("turn led {} {}".format(msg_json["led"], msg_json["mac"]),
                                                                      datetime.now().strftime("%H:%M:%S")))

            for each in self.disp:
                if each["mac"] == msg_json["mac"]:
                    each["led"] = msg_json["led"]

    def change_gpio(self, msg):
        if msg.topic == "front/gpios":
            msg_json = json.loads(msg.payload.decode())
            for each in self.apa:
                if each["aparelho"] == msg_json["aparelho"]:
                    self.save_command("{}, {}\n".format("change: {} status:{} ".format(msg_json["aparelho"], each["status"]),
                                                                                     datetime.now().strftime("%H:%M:%S")))
                    self.gpios.change_output(msg_json["aparelho"])
            
            self.apa = self.gpios.get_all_status()

    def check_alarm(self, msg):
        if msg.topic == "front/alarm":
            msg_json = json.loads(msg.payload.decode())
            self.save_command("{}, {}\n".format("change alarm status {}".format(msg_json["button"]), datetime.now().strftime("%H:%M:%S")))
            self.alarm = msg_json["button"]
        else:
            self.alarm = self.alarm_cli.sing(self.apa, self.alarm)

    def on_message(self, client, userdata, msg):
        print("---------\n{}\n{}\n--------".format(msg.topic, msg.payload.decode()))
        self.mac_verify(msg)
        self.init_component(msg)
        self.verify_room(msg)
        self.refresh_room(msg)
        self.turn_led(msg)
        self.change_gpio(msg)
        self.check_alarm(msg)
            

    def init_instance(self):
        self.client = mqtt.Client(transport="websockets")
        self.client.on_message = self.on_message

    def connect_to_broker(self, broker_addr):
        self.client.connect(broker_addr, 80, 60)

    def subscribe_topic(self, topic):
        try:
            self.client.subscribe(topic)
            self.available_topics.append(topic)
            self.available_topics = list(set(self.available_topics))
        except Exception as erro:
            print(erro)

    def publish_on_topic(self, topic, payload):
        self.client.publish(topic, payload)

    def load_list_of_topic(self, list_topic):
        self.available_topics = [each for each in list_topic]

    def load_connected_brokers(self, connect):
        self.connected_brokers = [each for each in connect]

    def load_disp(self, disp):
        self.disp = [each for each in disp]

    def connect_with_topics(self):
        print(self.available_topics)
        for each in self.available_topics:
            print("Recarregando {}".format(each))
            self.subscribe_topic(each)
