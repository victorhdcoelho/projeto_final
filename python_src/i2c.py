import bme280
import smbus2


class I2CUtils:
    def __init__(self, port, addr):
        self.port=port
        self.addr=addr
        self.bus=None
        self.params=None

    def init_and_calibrate_i2c(self):
        self.bus = smbus2.SMBus(self.port)
        self.params = bme280.load_calibration_params(self.bus, self.addr)

    def get_local_temp(self):
        data = bme280.sample(self.bus, self.addr, self.params)
        return {"temp": "{:.2f}".format(data.temperature), "hum": "{:.2f}".format(data.humidity)}
