#ifndef LED_H_
#define LED_H_

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "driver/gpio.h"

#include "mqtt.h"

#define LED_TAG "Led"
#define BLUE_LED 2

void led_start();

void led_on();

void led_off();

bool get_led_state();

#endif // LED_H_
