#ifndef BUTTON_H_
#define BUTTON_H_

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "driver/gpio.h"

#include "mqtt.h"

#define BUTTON 0
#define BUTTON_TAG "Button"

void button_interruption_handler(void *pvRoom);

void button_start();

bool get_button_state();

#endif // BUTTON_H_
