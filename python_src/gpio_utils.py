import RPi.GPIO as GPIO

class GPIOUtils:
    def __init__(self):
        self.gpios = {
            "LAMP1": {
                "num": 11,
                "status": 0,
            },
            "LAMP2": {
                "num": 12,
                "status": 0,
            },
            "SALA": {
                "num": 22,
                "status": 0,
            },
            "COZINHA": {
                "num": 37,
                "status": 0,
            },
            "PORTA_COZINHA": {
                "num": 29,
                "status": 0,    
            },
            "JANELA_COZINHA": {
                "num": 31,
                "status": 0,
            },
            "PORTA_SALA": {
                "num": 32,
                "status": 0,
            },
            "JANELA_SALA": {
                "num": 36,
                "status": 0
            },    
        }

    def init_all_gpios(self):
        GPIO.setmode(GPIO.BOARD)
        for key in self.gpios.keys():
            GPIO.setup(self.gpios[key]["num"], GPIO.OUT)
            GPIO.output(self.gpios[key]["num"], GPIO.LOW)

    def get_all_status(self):
        format_output = []
        for key in self.gpios.keys():
            self.gpios[key]["status"] = GPIO.input(self.gpios[key]["num"])
            format_output.append({"aparelho": key, "status": self.gpios[key]["status"]})
        return format_output

    def change_output(self, key):
        if self.gpios[key]["status"] == GPIO.LOW:
            GPIO.output(self.gpios[key]["num"], GPIO.HIGH)
        else:
            GPIO.output(self.gpios[key]["num"], GPIO.LOW)