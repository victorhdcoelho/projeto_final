#include "registration.h"

extern xSemaphoreHandle register_sem;  // Defined in main
extern esp_mqtt_client_handle_t client;  // Defined in mqtt.c
extern char exclusive_topic[EXCLUSIVE_TOPIC_SIZE]; // fse2020/150129815/dispositivos/24:62:AB:E1:9B:3C

char acquired_room[MAX_ROOM_LEN];

// Returns ESP_OK or ESP_ERR_NVS_NOT_FOUND if not registered
esp_err_t read_room(char *room) {
    ESP_ERROR_CHECK(nvs_flash_init_partition(REGISTRATION_PARTITION));

    nvs_handle partition_handle;
    esp_err_t read_status = nvs_open(REGISTRATION_PARTITION, NVS_READONLY, &partition_handle);

    if(read_status != ESP_OK && read_status != ESP_ERR_NVS_NOT_FOUND) {
        ESP_ERROR_CHECK(read_status);
    }

    size_t size = MAX_ROOM_LEN;
    read_status = nvs_get_str(partition_handle, ROOM_NVS_KEY, room, &size);
    
    nvs_close(partition_handle);

    return read_status;
}

void write_registration(char *room) {
    ESP_ERROR_CHECK(nvs_flash_init_partition(REGISTRATION_PARTITION));

    nvs_handle partition_handle;

    ESP_ERROR_CHECK(nvs_open(REGISTRATION_PARTITION, NVS_READWRITE, &partition_handle));
    
    ESP_ERROR_CHECK(nvs_set_str(partition_handle, ROOM_NVS_KEY, room));

    nvs_commit(partition_handle);
    nvs_close(partition_handle);
}

void register_device(char *room) {
    if(esp_mqtt_client_publish(client, exclusive_topic, "{\"register\": -1}", 0, 2, 0) < 0) {
        ESP_LOGE(REGISTRATION_TAG, "Failed to request the register!");
        return;
    }

    ESP_LOGI(REGISTRATION_TAG, "Waiting to be registered");
    xSemaphoreTake(register_sem, portMAX_DELAY);

    strcpy(room, acquired_room);
    ESP_LOGI(REGISTRATION_TAG, "Device registered to room \"%s\"!", room);
}

// If not registered, it register first
void fetch_registered_room(char *room) {
    esp_err_t read_status = read_room(room);

    if(read_status == ESP_OK) {  // Not registered, registering
        ESP_LOGI(REGISTRATION_TAG, "Device already registered to room \"%s\"", room);
    }
    else {  // ESP_ERR_NVS_NOT_FOUND
        ESP_LOGI(REGISTRATION_TAG, "Device not registered, registering");
        register_device(room);

        ESP_LOGD(REGISTRATION_TAG, "Persisting registration...");
        write_registration(room);
        ESP_LOGD(REGISTRATION_TAG, "Registration saved!");
    }
}
