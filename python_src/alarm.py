import subprocess
import threading


class AlarmUtils:
    def __init__(self):
        pass

    def check_status(self, aparelhos):
        for aparelho in aparelhos[2:]:
            if aparelho["status"] == 1:
               return True
        return False

    def sing(self, aparelhos, button):
        status = self.check_status(aparelhos)
        if status or button == "ON":
            alarm = subprocess.Popen(["omxplayer", "alarme.mp3"],
                                         stdout=subprocess.PIPE,
                                         stdin=subprocess.PIPE)
            print("Tocando alarme")
            return "ON"
        return "OFF"
