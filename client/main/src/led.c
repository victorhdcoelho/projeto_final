#include "led.h"

// #define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

void led_start(){
    gpio_reset_pin(BLUE_LED);
    gpio_set_direction(BLUE_LED, GPIO_MODE_INPUT_OUTPUT);
}

void led_on() {
    gpio_set_level(BLUE_LED, 1);
}

void led_off() {
    gpio_set_level(BLUE_LED, 0);
}

bool get_led_state() {
    return gpio_get_level(BLUE_LED) == 1;
}
