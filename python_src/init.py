import time
import json
import pickle
from mqqt_cliet import MqttClient
from i2c import I2CUtils


def save_jsons(post_dict):
    with open("checkout.json", "w") as j:
        j.write(post_dict)

def create_csv():
    with open("log.csv", "w") as f:
        f.write("comando,tempo\n")

if __name__ == "__main__":
    post_dict = {"disp": None, "apa": None, "top": None}
    mac_post = {"mac": "python"}
    create_csv()
    i2c = I2CUtils(1, 0x76)
    i2c.init_and_calibrate_i2c()
    mqtt_client = MqttClient()
    mqtt_client.init_instance()
    mqtt_client.connect_to_broker("mqtt.eclipseprojects.io")
    try:
        j = open("checkout.json")
        content = j.read()
        new_dict = json.loads(content)
        mqtt_client.load_disp(new_dict["como"])
        mqtt_client.load_list_of_topic(new_dict["top"])
        mqtt_client.load_connected_brokers(new_dict["disp"])
        mqtt_client.connect_with_topics()
        j.close()
    except IOError:
        mqtt_client.subscribe_topic("front/first")
        mqtt_client.subscribe_topic("front/mac")
        mqtt_client.subscribe_topic("front/turnled")
        mqtt_client.subscribe_topic("front/gpios")
        mqtt_client.subscribe_topic("front/alarm")
        mqtt_client.subscribe_topic("front/local_temp")
        mqtt_client.subscribe_topic("fse2020/150129815/dispositivos/#")
        

    print(mqtt_client.disp)
    print(mqtt_client.available_topics)
    print(mqtt_client.connected_brokers)
    mqtt_client.client.loop_start()
    while(True):
        time.sleep(2)
        try:
            post_dict["disp"] = mqtt_client.connected_brokers
            post_dict["top"] = mqtt_client.available_topics
            post_dict["como"] = mqtt_client.disp
            post_dict["apa"] = mqtt_client.apa
            post_dict["local_temp"] = i2c.get_local_temp()
            post_dict["alarm"] = mqtt_client.alarm
            print(post_dict)
            msg = json.dumps(post_dict)
            mac = json.dumps(mac_post)
            mqtt_client.publish_on_topic("front/first", msg)
            mqtt_client.publish_on_topic("front/mac", mac)
            save_jsons(msg)

        except Exception as erro:
            print(erro)
            mqtt_client.client.loop_stop()
            