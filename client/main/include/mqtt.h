#ifndef MQTT_H
#define MQTT_H

#include <string.h>
#include <assert.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "nvs_flash.h"
#include "cJSON.h"

#include "led.h"
#include "mqtt_client.h"

#define BROKER_URL "mqtt://mqtt.eclipseprojects.io"
#define MATRICULA "150129815"
#define TOPIC_PREFIX "fse2020"
#define MAX_ROOM_LEN 31
#define MQTT_TAG "MQTT"
#define EXCLUSIVE_TOPIC_SIZE 49
#define LED_KEY "saida"
#define BUTTON_KEY "entrada"

void mqtt_start();

void mqtt_handle_registration(esp_mqtt_event_handle_t event);

void mqtt_publish_temperature(char *comodo, float temperature);

void mqtt_publish_humidity(char *comodo, float humidity);

void mqtt_publish_button(char *room, bool state);

void mqtt_publish_state(char *room, bool input_state, bool output_state);

void format_topic(char *comodo, char *attribute, char *topic);

void get_mac(char *mac);

void get_exclusive_topic(char *topic);

#endif
