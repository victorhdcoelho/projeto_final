#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"
#include "nvs_flash.h"

#include "mqtt.h"

#define REGISTRATION_PARTITION "registration"
#define ROOM_NVS_KEY "room"
#define REGISTRATION_TAG "Registration"

void fetch_registered_room(char *room);

void register_device();

void write_registration(char *room);

esp_err_t read_room(char *room);

#endif
