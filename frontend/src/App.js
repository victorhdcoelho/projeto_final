import React, { Component } from 'react';
import './App.css';
import mqtt from 'mqtt';
import Navbar from "react-bootstrap/Navbar";
import Alert from "react-bootstrap/Alert";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table"
import "bootstrap/dist/css/bootstrap.min.css";



class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      receive: {
        "disp": [{"mac": "sem", "date": "sem"}],
        "apa": [],
        "top": [],
        "como": [],
        "local_temp": {"temp": 0, "hum": 0},
        "alarm": "OFF",
      },
    }
    this.client = mqtt.connect("ws://mqtt.eclipseprojects.io/mqtt");
    this.getMessage = this.getMessage.bind(this);
    this.clickToChangeLed = this.clickToChangeLed.bind(this);
    this.clickToChangeGPIO = this.clickToChangeGPIO.bind(this);
    this.clickAlarm = this.clickAlarm.bind(this);
  }

  clickAlarm(){
    var url = "front/alarm";
    var payload = {"button": ""}; 
    if (this.state.receive.alarm === "OFF"){
      payload["button"] = "ON";
    }
    else{
      payload["button"] = "OFF";
    }
    this.client.publish(url, JSON.stringify(payload));
  }

  clickToChangeGPIO(value){
    var url = "front/gpios";
    var paylaod = {"aparelho": value.aparelho};
    this.client.publish(url, JSON.stringify(paylaod));
  }

  clickToChangeLed(value){
      var url = "fse2020/150129815/dispositivos/"+value.mac;
      var logical = "front/turnled";
      console.log(url);
      var paylaod = {"saida": ""};
      var payload2 = {"mac": value.mac, "led": value.led}
      if (value.led === "OFF"){
        paylaod["saida"] = "ON";
        payload2["led"] = "ON";
      }
      else{
        paylaod["saida"] = "OFF";
        payload2["led"] = "OFF";
      }
      this.client.publish(url, JSON.stringify(paylaod), {"qos": 1});
      this.client.publish(logical, JSON.stringify(payload2));
  }

  getMessage(){
    var self = this;
    this.client.subscribe('front/first');
    this.client.subscribe('front/mac');
    this.client.subscribe('front/local_temp');
    this.client.subscribe('front/turnled');
    this.client.subscribe('front/gpios');
    this.client.subscribe('front/alarm');
    this.client.subscribe('fse2020/150129815/dispositivos/#');
    var cli = this.client;
    cli.on('message', function (topic, message) {
      if(topic.includes("fse2020/150129815/dispositivos/")){
        var json_msg = JSON.parse(message);
        if(!('room' in json_msg) && !('saida' in json_msg)){
        alert("New device found !");
        var name = prompt("enter device name: ");
        var msg = {"room": name};
        console.log(msg);
        cli.publish(topic, JSON.stringify(msg), {'qos': 2});
        }
        console.log("nothing to do 2 !");
      }
      if(topic === 'front/first'){
      self.setState({receive: JSON.parse(message)});
      console.log(JSON.parse(message));
      setTimeout(() => {
        console.log("wait");
      }, 10);
    }})
  } 

  componentDidMount(){
    this.getMessage()
  }

  render() {
    return (
      <div>
      <Navbar bg="dark" expand="lg" >
        <Navbar.Brand href="#" style={{color: 'white'}}>FSE</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
      </Navbar>
    <div style={{marginTop: "10px"}}>
      <Container fluid>
        <Row>
          <Col xs={6} md={4}>
          <Alert key="dispositivos" variant="secondary">
            Dispositivos conectados:
          </Alert>
          <Table striped bordered hover>
          <thead>
              <tr>
                <th>MAC</th>
                <th>Data e hora (att)</th>
              </tr>
            </thead>
            <tbody>
          {this.state.receive.disp.map((value, index)=>{
            return <tr><td > {value.mac}</td><td>{value.date}</td></tr>
          })}
          </tbody>
          </Table>
          </Col>
          <Col>
            <Alert key="aparelhos" variant="secondary">
              Aparelhos e status:
            </Alert>

            <Table striped bordered hover>
          <thead>
              <tr>
                <th>Aparelho</th>
                <th>Status</th>
                <th>Botão</th>
              </tr>
            </thead>
            <tbody>
          {this.state.receive.apa.map((value, index)=>{
            return <tr><td > {value.aparelho}</td><td>{value.status}</td> <td><Button onClick={(e) => this.clickToChangeGPIO(value, e)}>ON/OFF</Button></td></tr>
          })}
          <tr><td >Alarme</td><td>{this.state.receive.alarm}</td> <td><Button onClick={(e) => this.clickAlarm(e)}>ON/OFF</Button></td></tr>
          </tbody>
          </Table>
          </Col>

          <Col>
            <Alert key="topicos" variant="secondary">
              Tópicos disponíveis:
              </Alert>

              <Table striped bordered hover>
          <thead>
              <tr>
                <th>Tópicos</th>
              </tr>
            </thead>
            <tbody>
          {this.state.receive.top.map((value, index)=>{
            if(!value.includes("front/")){
            return <tr><td > {value}</td></tr>
            }
          })}
          </tbody>
          </Table>
          </Col>
          <Col>
            <Alert key="local_temp" variant="secondary">
              Temperatura Local:
            </Alert>
            <Table striped bordered hover>
          <thead>
              <tr>
                <th>Temperatura</th>
                <th>Umidade</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td > {this.state.receive.local_temp.temp}</td>
                <td>{this.state.receive.local_temp.hum}</td>
              </tr>
          </tbody>
          </Table>
          </Col>
          </Row>
          <Row>
            <Col>
            <Alert key="mapeamento" variant="secondary">
              Mapeamento de dispositivos:
              </Alert>
              <Table striped bordered hover>
          <thead>
              <tr>
                <th>Comodo</th>
                <th>MAC</th>
                <th>Temperatura</th>
                <th>Humidade</th>
                <th>Entrada</th>
                <th>Saída</th>
              </tr>
            </thead>
            <tbody>
          {this.state.receive.como.map((value, index)=>{
            return <tr><td>{value.room}</td> <td > {value.mac}</td><td>{value.temperatura}</td><td>{value.umidade}</td><td>{value.status}</td><td>{value.led}<br/><Button onClick={(e) => this.clickToChangeLed(value, e)}>ON/OFF</Button></td></tr>
          })}
          </tbody>
            </Table>
            </Col>
          </Row>
        </Container>
        </div>
        </div>
    );
  }
}

export default App;
