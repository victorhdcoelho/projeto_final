#include "button.h"

static xSemaphoreHandle button_interrupt_sem;

bool get_button_state() {
    return gpio_get_level(BUTTON) == 0;
}

static void IRAM_ATTR gpio_isr_handler(void *args) {
    xSemaphoreGiveFromISR(button_interrupt_sem, pdFALSE);
}

void button_interruption_handler(void *pvRoom) {
    char *room = (char *) pvRoom;

    while(true) {
        xSemaphoreTake(button_interrupt_sem, portMAX_DELAY);

        ESP_LOGD(BUTTON_TAG, "Button pressed");
        gpio_isr_handler_remove(BUTTON); // De-bouncing

        mqtt_publish_button(room, true);  // pressed

        while(!gpio_get_level(BUTTON)) {
            vTaskDelay(50 / portTICK_PERIOD_MS);
        }

        ESP_LOGD(BUTTON_TAG, "Button released");
        mqtt_publish_button(room, false); // released

        gpio_isr_handler_add(BUTTON, gpio_isr_handler, NULL);
    }
}

void button_start() {
    gpio_pad_select_gpio(BUTTON);
    gpio_set_direction(BUTTON, GPIO_MODE_INPUT);

    ESP_ERROR_CHECK(gpio_pulldown_dis(BUTTON));
    ESP_ERROR_CHECK(gpio_pullup_en(BUTTON));

    gpio_set_intr_type(BUTTON, GPIO_INTR_NEGEDGE);

    button_interrupt_sem = xSemaphoreCreateBinary();

    gpio_install_isr_service(0);
    gpio_isr_handler_add(BUTTON, gpio_isr_handler, NULL);
}
