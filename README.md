# Projeto Final - Fundamentos de Sistemas Embarcados

|Nome| Matrícula|
|---|---|
|Icaro Pires de Souza Aragão|150129815|
|Victor Hugo Dias Coelho|160019401|

A aplicação como um todo possui três componentes: servidor central (em Python), cliente(s) (em C) e frontend (em JS). A comunicação entre o servidor central e o(s) cliente(s) obedece ao especificado [aqui](https://gitlab.com/fse_fga/projetos/trabalho-final), e a comunicação entre servidor central e o frontend foi definida pelos desenvolvedores. Em alguns casos, o frontend se comunica diretamente com o cliente, como no caso de cadastro de novo cliente. Mas, no geral, a comunicação acontece da seguinte forma: Frontend \<-\> Servidor Central \<-\> Cliente (s), utilizando sempre o protocolo de comunicação MQTT.

Visão geral dos módulos:

* **Servidor Central**: Monitora a situação como um todo, persiste informações, soa o alarme e mantém log em `csv`;
* **Frontend:** Interface web para visualização do estado do sistema como um todo, interação com o usuário e cadastro de novos dispositivos;
* **Cliente(s):** Dispositivos ESP32 conectados a um dispositivo de entrada, um dispositivo de saída e um sensor DHT11;

## Quickstart

## Clone

Este repositório possui submódulos, então para cloná-lo, utilize o comando:
``` bash
git clone --recursive https://gitlab.com/victorhdcoelho/projeto_final
```

## Execução e build

**Importante:** O cliente deve ser executado por último, com o servidor central e o frontend já em execução. Ordem recomendada de execução: frontend, servidor central, cliente(s).

### Servidor Central

* Acesse o servidor (Raspberry Pi) utilizando o comando: `ssh -L 3000:localhost:3000 <user>@3.tcp.ngrok.io -p 23900`;
* Clone o repositório, conforme instruções anteriories;
* No diretório `frontend`, utilize o comando `docker-compose up frontend`, para subir o frontend;
    * Observação: Se ao subir o frontend acontecer algum timeout, geralmente a realização de uma nova tentativa resolve o problema;
* Espere o docker do frontend subir;
* No diretório `python_src`, instale as dependências com `pip3 install -r requirements.txt --user` (apenas no primeiro uso);
* No diretório `python_src`, utilize o comando `python3 init.py` para iniciar o servidor central;
* Acesse `http://localhost:3000` no browser, ou use este link: [localhost:3000](http://localhost:3000);
* Aguarde a interface carregar e estará pronto para uso;

### Cliente

O cliente será executado na esp32. Considerando que o esp-idf já está configurado e pronto para uso, para realizar o build, execute os seguintes passos:

* Entre na pasta `client`;

* Execute `idf.py menuconfig`, acesse a opção `Partitions Table` e cheque se a configuração está conforme a figura:

![parition_tables](./docs/partition_table.png)

* Saia do `menuconfig` salvando as configurações;

* Por fim, execute o seguinte comando:

``` bash
idf.py flash monitor  # Irá compilar, fazer o flash e iniciar monitoramento
```

## Resetando configurações

Alguns dados são persistentes, tanto no servidor central como no(s) cliente(s). Para apagá-los, siga os passos descritos nas próximas seções.

### Cliente

Comandos:
``` bash
# A partir da raiz do repositório
cd client
idf.py erase_flash
```

### Servidor Central

* Apague os arquivos `python_src/checkout.json` e `python_src/log.csv`

## Power Saving Mode

Também foi implementado o recurso de Low Power, para economizar energia. Ele vem desativado por padrão e pode ser ativado no seguinte caminho:

* Execute o comando: `idf.py menuconfig`;
* Vá no menu `Power Saving`;
* Cheque a opção `Power Saving`;
* Refaça o build do projeto;

**Alerta:** Alguns eventos só vão reagir quando o cliente "acordar", periodicamente, do modo low power;
