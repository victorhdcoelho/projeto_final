#include "mqtt.h"

// #define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

extern xSemaphoreHandle connection_wifi_sem;
extern xSemaphoreHandle register_sem;
esp_mqtt_client_handle_t client;

// Defined in registration.c
extern char acquired_room[MAX_ROOM_LEN];

char exclusive_topic[EXCLUSIVE_TOPIC_SIZE]; // fse2020/150129815/dispositivos/24:62:AB:E1:9B:3C

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event) {
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGD(MQTT_TAG, "MQTT_EVENT_CONNECTED");
            ESP_LOGI(MQTT_TAG, "Connected to MQTT broker!");
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGD(MQTT_TAG, "MQTT_EVENT_DISCONNECTED");
            ESP_LOGW(MQTT_TAG, "Disconnected from MQTT broker!");
            mqtt_start();
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGD(MQTT_TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGD(MQTT_TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGD(MQTT_TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGD(MQTT_TAG, "MQTT_EVENT_DATA");

            if(strcmp(event->topic, exclusive_topic) == 0) {
                cJSON *response = cJSON_Parse(event->data);

                cJSON *room_ref = cJSON_GetObjectItem(response, "room");
                cJSON *led_ref = cJSON_GetObjectItem(response, LED_KEY);

                if(room_ref != NULL) {  // Register
                    strcpy(acquired_room, room_ref->valuestring);

                    xSemaphoreGive(register_sem);
                }
                else if (led_ref != NULL) {  // Control LED
                    if (strcmp(led_ref->valuestring, "ON") == 0) {
                        ESP_LOGI(MQTT_TAG, "Turning LED ON");
                        led_on();
                    }
                    else if (strcmp(led_ref->valuestring, "OFF") == 0) {
                        ESP_LOGI(MQTT_TAG, "Turning LED OFF");
                        led_off();
                    }
                    else {
                        ESP_LOGE(MQTT_TAG, "Invalid option for LED: %s", led_ref->valuestring);
                    }

                }

                cJSON_Delete(response);
            }

            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGD(MQTT_TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGD(MQTT_TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(MQTT_TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

void mqtt_start() {
    static bool is_first_connection = true;

    if(is_first_connection) {
        ESP_LOGI(MQTT_TAG, "Making first connection to MQTT broker at this runtime");

        xSemaphoreTake(connection_wifi_sem, portMAX_DELAY);
        is_first_connection = false;
    }
    else {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        ESP_LOGI(MQTT_TAG, "Trying to reconnect to broker");
    }

    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = BROKER_URL,
    };

    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);

    get_exclusive_topic(exclusive_topic);

    if(esp_mqtt_client_subscribe(client, exclusive_topic, 1) < 0) {
        ESP_LOGE(MQTT_TAG, "Failed to subscribe to exclusive topic");
    }
}

void mqtt_publish_temperature(char *room, float temperature) {
    char msg[23]; // Ex: {"temperatura": 20.50};
    sprintf(msg, "{\"temperatura\": %0.2f}", temperature);

    char topic[50];
    format_topic(room, "temperatura", topic);

    ESP_LOGI(MQTT_TAG, "Publishing temperature: %0.2f", temperature);
    esp_mqtt_client_publish(client, topic, msg, 0, 0, 0);
}

void mqtt_publish_humidity(char *room, float humidity) {
    char msg[20]; // Ex: {"umidade": 100.00};
    sprintf(msg, "{\"umidade\": %0.2f}", humidity);

    char topic[50];
    format_topic(room, "umidade", topic);

    ESP_LOGI(MQTT_TAG, "Publishing humidity: %0.2f", humidity);
    esp_mqtt_client_publish(client, topic, msg, 0, 0, 0);
}

void mqtt_publish_state(char *room, bool input_state, bool output_state) {
    char msg[35]; // Ex: {"entrada": "OFF", "saida": "OFF"};

    char input_state_str[4], output_state_str[4];

    if(input_state) {
        strcpy(input_state_str, "ON");
    } else {
        strcpy(input_state_str, "OFF");
    }

    if(output_state) {
        strcpy(output_state_str, "ON");
    } else {
        strcpy(output_state_str, "OFF");
    }
    sprintf(msg, "{\"%s\": \"%s\", \"%s\": \"%s\"}", BUTTON_KEY, input_state_str, LED_KEY, output_state_str);

    char topic[50];
    format_topic(room, "estado", topic);

    ESP_LOGI(MQTT_TAG, "Publishing state: %s", msg);
    esp_mqtt_client_publish(client, topic, msg, 0, 0, 0);
}

void mqtt_publish_button(char *room, bool state) {
    char msg[19]; // Ex: {"entrada": "OFF"};

    char state_str[4];
    if (state) {
        strcpy(state_str, "ON");
    } else {
        strcpy(state_str, "OFF");
    }
    sprintf(msg, "{\"%s\": \"%s\"}", BUTTON_KEY, state_str);

    char topic[50];
    format_topic(room, "estado", topic);

    ESP_LOGI(MQTT_TAG, "Publishing button state event: %s", state_str);
    esp_mqtt_client_publish(client, topic, msg, 0, 1, 0);
}

// Ex: fse2020/150129815/cozinha/temperatura
void format_topic(char *room, char *attribute, char *topic) {
    sprintf(topic, "%s/%s/%s/%s", TOPIC_PREFIX, MATRICULA, room, attribute);
}

void get_mac(char *mac) {
    uint8_t mac_arr[6];
    ESP_ERROR_CHECK(esp_read_mac(mac_arr, ESP_MAC_WIFI_STA));

    sprintf(mac, "%2X:%2X:%2X:%2X:%2X:%2X", mac_arr[0], mac_arr[1], mac_arr[2], mac_arr[3], mac_arr[4], mac_arr[5]);
}

void get_exclusive_topic(char *topic) {
    char mac[18]; // 24:62:AB:E1:9B:3C
    get_mac((char *) mac);

    sprintf(topic, "%s/%s/dispositivos/%s", TOPIC_PREFIX, MATRICULA, mac);
}
