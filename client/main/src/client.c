#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "esp32/rom/uart.h"

#include "dht11.h"

#include "wifi.h"
#include "mqtt.h"
#include "registration.h"

#include "button.h"
#include "led.h"

#ifndef CONFIG_POWER_SAVING
#define CONFIG_POWER_SAVING 0
#endif

#define POWER_SAVING CONFIG_POWER_SAVING
#define CLIENT_TAG "Client"
#define PUBLISH_INTERVAL 30 // s

xSemaphoreHandle register_sem;
xSemaphoreHandle connection_wifi_sem;

void update_attributes(void *pvRoom) {
    DHT11_init(GPIO_NUM_4);

    char *room = (char *) pvRoom;

    ESP_LOGI(MQTT_TAG, "Started publising state every %d s", PUBLISH_INTERVAL);

    if (POWER_SAVING) {
        // -1 because is waited 1s before power saving mode
        ESP_ERROR_CHECK(esp_sleep_enable_timer_wakeup((PUBLISH_INTERVAL - 1) * 1000000));
    }

    while(true) {
        struct dht11_reading weather = DHT11_read();

        if(weather.status < 0) {
            ESP_LOGD(CLIENT_TAG, "Failed DHT11 reading with code %d", weather.status);
            continue;
        }

        mqtt_publish_temperature(room, (float) weather.temperature);
        mqtt_publish_humidity(room, (float) weather.humidity);

        bool output_state = get_led_state();
        bool input_state = get_button_state();
        mqtt_publish_state(room, input_state, output_state);

        if (!POWER_SAVING) {
            vTaskDelay(PUBLISH_INTERVAL * 1000 / portTICK_PERIOD_MS);
        } else {
            vTaskDelay(1000 / portTICK_PERIOD_MS);  // Wait everything be published

            ESP_LOGI(CLIENT_TAG, "Entering light sleep");
            uart_tx_wait_idle(CONFIG_ESP_CONSOLE_UART_NUM);

            esp_light_sleep_start();

            ESP_LOGI(CLIENT_TAG, "Woke up!");
        }
    }
}

void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    connection_wifi_sem = xSemaphoreCreateBinary();
    register_sem = xSemaphoreCreateBinary();

    wifi_start();
    mqtt_start();  // After Wi-Fi start
    led_start();  // After MQTT start

    char room[MAX_ROOM_LEN];
    fetch_registered_room(room);  // After MQTT start

    button_start(room);  // After MQTT start

    xTaskCreate(&update_attributes, "Update hardware state to MQTT", 4096, (void *) room, 5, NULL);  // After MQTT
    xTaskCreate(&button_interruption_handler, "Button handler", 2048, (void *) room, 10, NULL);
}
